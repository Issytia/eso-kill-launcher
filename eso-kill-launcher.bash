#!/bin/bash

<<LICENSE
    Copyright (C) 2022  Issytia

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
LICENSE

# inspired by https://github.com/kevinlekiller/eso-linux-launcher/blob/master/ell.sh

_launcher="/bufferselfpatchfix /steam true"

function kill_launcher() {
	_time=0
	while [[ $(pgrep -f "$_launcher") == "" ]]; do
		if [[ $_time -ge 120 ]]; then
			exit 1
		fi
		sleep 1s
		((_time++))
	done

	_time=0
	while [[ $(pgrep -f "eso64.exe") == "" ]]; do
		if [[ $(pgrep -f "$_launcher") == "" ]]; then
			exit 0
		fi
		if [[ $_time -ge 3600 ]]; then
			pkill -f "$_launcher"
			exit 1
		fi
		sleep 1s
		((_time++))
	done

	pkill -f "$_launcher"
}

kill_launcher &
exec "$@"
